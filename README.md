Db2 Initial Lookup
Created by: Michal Pukala


----
1. Run script which points db2support unpacked directory as first parameter
./db2il.sh <db2support_unpacked_path>
2. Report file will be generated in current directory with name 'il_report_*'

Example report:
Initial report of diagnostics
------- ------ -- -----------
Db2 Version: DB2 v11.5.6.0", "s2106111000", "DYN2106111000AMD64",

OS Version: Linux 5 4   Ubuntu 18.04

Machine resources: TotalCPU 6, Physical Total Memory 14645 MB, Physical Free Memory 11709 MB, Virtual Memory Total 14645 MB, Virtual Memory Free 14645 MB

Number of nodes: 1

Active Databases: Total 2,

Instance settings: DiagLevel 3, Federated NO

Database LOGs: Log file size 1000, Log Primary 3, Log Secondary 30, Fist Active log S0000065.LOG

Log Paths: Active log path /home/d11560/d11560/NODE0000/SQL00002/LOGSTREAM0000/, Archive method LOGRETAIN, Archive method 2 OFF

Log Space needed: 132 MB

Memory: Instance memory AUTOMATIC(3203650), STMM OFF, DB Memory AUTOMATIC(83008), App Heap AUTOMATIC(256)

HADR: HADR not defined


Source files
------ ----
