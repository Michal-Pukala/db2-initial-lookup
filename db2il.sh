#!/bin/bash

DB2SUPPORT_FILE=$1

case $1 in
"")
    echo "Point the db2support unpacked directory as FIRST argument"
    ;;
*)
  #Create report file
  touch ${PWD}/il_report_$(date +%F_%T).out
  REPORTFILE=$(ls -t . | grep "il_report_" | head -1)

  #Source files
  WCMEM=$(ls -l $DB2SUPPORT_FILE/DB2CONFIG | grep "[[:digit:]]_db.supp_cfg" | wc -l)
  #Members
  if [ $(find $DB2SUPPORT_FILE -name "*member1_db.supp_cfg") ]
  then
    #More than 1 member
    declare -a DB_CFG
    WCMEM=$(ls -l $DB2SUPPORT_FILE/DB2CONFIG | grep "[[:digit:]]_db.supp_cfg" | wc -l)
    for (( c=0; c<$WCMEM; c++ ))
    do
      ACTUALDBCFG=$(find $DB2SUPPORT_FILE/DB2CONFIG -name "*member${c}_db.supp_cfg")
      DB_CFG[$c]=$ACTUALDBCFG
    done
  else
    DB_CFG[0]=$(find $DB2SUPPORT_FILE -name "*member0_db.supp_cfg")
  fi

  if [ $WCMEM -gt 0 ]
  then
    #More than 1 members
    declare -a LOGSIZE
    declare -a LOGPRIM
    declare -a LOGSECOND
    declare -a LOGFIRST
    declare -a LOGPATH
    declare -a LOGPARCHIVE
    declare -a LOGPARCHIVE2
    declare -a LOGSPACE
    declare -a STMMMEM
    declare -a DBMEM
    declare -a APPHMEM
    for (( c=0; c<$WCMEM; c++ ))
    do
      #Database LOGs
      LOGSIZE[$c]=$(grep "LOGFILSIZ" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
      LOGPRIM[$c]=$(grep "LOGPRIMARY" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
      LOGSECOND[$c]=$(grep "LOGSECOND" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
      LOGFIRST[$c]=$(grep "First active log file" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
      LOGPATH[$c]=$(grep "Path to log files" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
      LOGPARCHIVE[$c]=$(grep "LOGARCHMETH1" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
      LOGPARCHIVE2[$c]=$(grep "LOGARCHMETH2" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
      LOGSPACE[$c]=$(echo "((${LOGSIZE[$c]}*4)*(${LOGPRIM[$c]}+${LOGSECOND[$c]}))/1000" | bc)
      #Datbase memory
      STMMMEM[$c]=$(grep "SELF_TUNING_MEM)" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
      DBMEM[$c]=$(grep "DATABASE_MEMORY)" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
      APPHMEM[$c]=$(grep "APPLHEAPSZ)" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
    done
  else
    #1 member
    #Database LOGs
    c=0
    LOGSIZE[$c]=$(grep "LOGFILSIZ" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
    LOGPRIM[$c]=$(grep "LOGPRIMARY" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
    LOGSECOND[$c]=$(grep "LOGSECOND" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
    LOGFIRST[$c]=$(grep "First active log file" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
    LOGPATH[$c]=$(grep "Path to log files" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
    LOGPARCHIVE[$c]=$(grep "LOGARCHMETH1" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
    LOGPARCHIVE2[$c]=$(grep "LOGARCHMETH2" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
    LOGSPACE[$c]=$(echo "((${LOGSIZE[$c]}*4)*(${LOGPRIM[$c]}+${LOGSECOND[$c]}))/1000" | bc)
    #Datbase memory
    STMMMEM[$c]=$(grep "SELF_TUNING_MEM)" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
    DBMEM[$c]=$(grep "DATABASE_MEMORY)" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
    APPHMEM[$c]=$(grep "APPLHEAPSZ)" ${DB_CFG[$c]} | cut -f2 -d "=" | xargs)
  fi

  #Instance wide
  DBM_CFG=$(find $DB2SUPPORT_FILE -name "*dbm.supp*")
  LEVEL=$(find $DB2SUPPORT_FILE -name "*db2level*")
  OSINFO=$(find $DB2SUPPORT_FILE -name "*osinfo*")
  NODES=$(find $DB2SUPPORT_FILE -name "*db2nodes.cfg.supp_cfg*")
  LICENSE=$(find $DB2SUPPORT_FILE -name "*db2licm.supp*")
  ACTIVEDB=$(find $DB2SUPPORT_FILE -name "*active_db*")

  #db2level
  LE1=$(grep 'Informational tokens are' $LEVEL | cut -f2-100 -d "\"")
  #OS INFO
  OSNAME=$(grep 'OSName' $OSINFO | cut -f2 -d ":" | xargs)
  OSVER=$(grep 'Version' $OSINFO | cut -f2 -d ":" | xargs)
  OSREL=$(grep 'Release' $OSINFO | cut -f2 -d ":" | xargs)
  OS1=$(grep 'Distros' $OSINFO | cut -f2 -d ":" | xargs)
  OSHOST=$(grep 'NodeName' $OSINFO | cut -f2 -d ":" | xargs)
  #Machine resources
  TOTALCPU=$(grep -A 1 'TotalCPU' $OSINFO | tail -1 | awk '{print $1}' | xargs)
  TOTALRAMMEM=$(grep -A 1 'TotalMem' $OSINFO | tail -1 | awk '{print $1}' | xargs)
  FREERAMMEM=$(grep -A 1 'FreeMem' $OSINFO | tail -1 | awk '{print $2}' | xargs)
  VIRTUALMEMTOTAL=$(grep -A 1 'Reserved' $OSINFO | tail -1 | awk '{print $1}' | xargs)
  VIRTUALMEMFREE=$(grep -A 1 'Reserved' $OSINFO | tail -1 | awk '{print $1}' | xargs)
  #NODES
  NODESN=$(grep 'Number of nodes' $NODES | cut -f2 -d ":" | xargs)

  #Active db
  TOTACTIVEDB=$(grep "Database name" $ACTIVEDB | wc -l)
  NOADB=$(grep "Database name" $ACTIVEDB | awk '{print $4}' | wc -l)

  if [ $TOTACTIVEDB -gt 0 ]
  then
    for (( c=0; c<$NOADB; c++ ))
    do
      heads=$((c+1))
      ACTIVEDBNAMECUR=$(grep "Database name" $ACTIVEDB | awk '{print $4}' | head -$heads)
    done
  fi
  NNAMES=$(echo $ACTIVEDBNAMECUR | awk 'BEGIN{ORS=""}1') > /dev/null

  #Instance configs
  DIAGLVL=$(grep "DIAGLEVEL" $DBM_CFG | cut -f2 -d "=" | xargs)
  FED=$(grep "FEDERATED)" $DBM_CFG | cut -f2 -d "=" | xargs)

  #Instance Memory
  INSMEM=$(grep "INSTANCE_MEMORY)" $DBM_CFG | cut -f2 -d "=" | xargs)

  #HADR db level
  CHECKHADR=$(grep "HADR_REMOTE_INST" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
    if [ -z $CHECKHADR ]
    then
      #NO HADR
      HADROUT=$(echo "HADR not defined")
    else
      #HADR
      HROLE=$(grep "HADR database role" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
      HSYNC=$(grep "HADR_SYNCMODE)" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
      HTOUT=$(grep "HADR_TIMEOUT)" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
      HPEER=$(grep "HADR_PEER_WINDOW)" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
      HRINST=$(grep "HADR_REMOTE_INST" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
      HTLIST=$(grep "HADR_TARGET_LIST)" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
      HLHOST=$(grep "HADR_LOCAL_HOST)" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
      HLSVC=$(grep "HADR_LOCAL_SVC)" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
      HRHOST=$(grep "HADR_REMOTE_HOST)" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
      HRSVC=$(grep "HADR_REMOTE_SVC)" ${DB_CFG[0]} | cut -f2 -d "=" | xargs)
      HADROUT=$(echo "HADR role $HROLE, Sync $HSYNC, Timeout $HTOUT, Peer window $HPEER, Remote Instance $HRINST, Target list $HTLIST, Local Host $HLHOST, Local Service $HLSVC, Remote host $HRHOST, Remote service $HRSVC")
    fi

  #Generate report
  echo "
  Initial report of diagnostics
  ------- ------ -- -----------
  " >> ${PWD}/$REPORTFILE
  #Report for all members
  for (( c=0; c<$WCMEM; c++ ))
  do
    echo "
    MEMBER $c
    Db2 Version: $LE1
    Hostname: $OSHOST
    OS Version: $OSNAME $OSVER $OSREL $OS1
    Machine resources: TotalCPU $TOTALCPU, Physical Total Memory $TOTALRAMMEM MB, Physical Free Memory $FREERAMMEM MB, Virtual Memory Total $VIRTUALMEMTOTAL MB, Virtual Memory Free $VIRTUALMEMFREE MB
    Number of nodes: $NODESN
    Active Databases: Total $TOTACTIVEDB, $NNAMES
    Instance settings: DiagLevel $DIAGLVL, Federated $FED
    Database LOGs: Log file size ${LOGSIZE[$c]}, Log Primary ${LOGPRIM[$c]}, Log Secondary ${LOGSECOND[$c]}, Fist Active log ${LOGFIRST[$c]}
    Log Paths: Active log path ${LOGPATH[$c]}, Archive method ${LOGPARCHIVE[$c]}, Archive method 2 ${LOGPARCHIVE2[$c]}
    Log Space needed: ${LOGSPACE[$c]} MB
    Memory: Instance memory ${INSMEM[$c]}, STMM ${STMMMEM[$c]}, DB Memory ${DBMEM[$c]}, App Heap ${APPHMEM[$c]}
    HADR: $HADROUT

    ---
    " >> ${PWD}/$REPORTFILE
  done

  #Source informations
#  cat $LEVEL >> ${PWD}/$REPORTFILE
#  cat $OSINFO >> ${PWD}/$REPORTFILE
#  cat $NODES >> ${PWD}/$REPORTFILE
#  cat $LICENSE >> ${PWD}/$REPORTFILE
#  cat $DBM_CFG >> ${PWD}/$REPORTFILE
  ;;
esac
